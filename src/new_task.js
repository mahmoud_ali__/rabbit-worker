#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
const http = require('http');
const { parse } = require('querystring');

const server = http.createServer((req, res) => {
    if (req.method === 'POST') {
        collectRequestData(req, result => {
            console.log(result);
            amqp.connect('amqp://localhost', function(error0, connection) {
                if (error0) {
                    throw error0;
                }
                connection.createChannel(function(error1, channel) {
                    if (error1) {
                        throw error1;
                    }
                    var queue = 'task_queue';
                    var msg = JSON.stringify(result);

                    channel.assertQueue(queue, {
                        durable: true
                    });
                    channel.sendToQueue(queue, Buffer.from(msg), {
                        persistent: true
                    });
                    console.log(" [x] Sent '%s'", msg);
                });
                // setTimeout(function() {
                //     connection.close();
                //     process.exit(0);
                // }, 500);
            });
            res.end(`Parsed data belonging to ${result.name}`);
        });
    } 
    else {
        res.end(`
            <!doctype html>
            <html>
            <body>
                Hello World!!!!
            </body>
            </html>
        `);
    }
});
server.listen(3000);

function collectRequestData(request, callback) {
    const FORM_URLENCODED = 'application/x-www-form-urlencoded';
    if(request.headers['content-type'] === FORM_URLENCODED) {
        let body = '';
        request.on('data', chunk => {
            body += chunk.toString();
        });
        request.on('end', () => {
            callback(parse(body));
        });
    }
    else {
        callback(null);
    }
}


