#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "mc050215",
  database: "procrew"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");

    amqp.connect('amqp://localhost', function(error, connection) {
        connection.createChannel(function(error, channel) {
            var queue = 'task_queue';
            channel.assertQueue(queue, {
                durable: true
            });
            channel.prefetch(1);
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
            channel.consume(queue, function(msg) {
                var secs = msg.content.toString().split('.').length - 1;
                console.log(msg.content.toString());
                var kk = JSON.parse(msg.content.toString());
                console.log(" [x] Received fname:%s", kk.name );
                setTimeout(function() {
                      var sql = "INSERT INTO users (name, phone, email) VALUES ('"+ kk.name +"', '"+ kk.phone +"', '"+ kk.email +"')";
                      con.query(sql, function (err, result) {
                        if (err) throw err;
                        console.log("1 record inserted");
                      });
                    
                    channel.ack(msg);
                }, secs * 1000);
            }, {
                noAck: false
            });
        });
    });
});
